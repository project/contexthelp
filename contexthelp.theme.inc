<?php
// $id

/**
 * Themes the contexthelp block that can be displayed on the page
 */
function theme_contexthelp_block() {
  global $user;
  $output = '';

  // check if the user has access to view the context help information
  if (user_access('context help view')) {
    $url = ($_GET['q']) ? $_GET['q'] : $_POST['q'];
    if ($node = contexthelp_get_help($url)) {
      // checking if the user has administrative access if so allow them to easily go to the edit screen
      if (user_access('administer context help')) {
        $output = '<div>'. l(t('Edit Help'), 'node/'. $node->nid .'/edit') .'</div>';
      }
      // calling the associated view so we can display the context help
      $output .= views_embed_view('contexthelp', NULL, $node->nid);
    }
    else {
      $output = t('No context help available for this section');
      if (user_access('administer context help')) {
        $contexthelp_type = variable_get('contexthelp_node_type', '');
        if (!is_null($contexthelp_type) && drupal_strlen($contexthelp_type) > 0) {
          $output .= '<div>'. l(t('Create Help'), 'node/add/'. str_replace("_", "-", $contexthelp_type), array('query' => array('url' => $url))) .'</div>';
        }
      }
    }
  }
  return $output;
}
