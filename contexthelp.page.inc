<?php
// $id

/**
 * Returns the page with all of the context help available in the system
 */
function contexthelp_list_page() {
  $output = '';

  // only display if we have the contexthelp node type specified in the module configuration
  if ($contexthelp_type = variable_get('contexthelp_node_type', '')) {
    // table headers we will display to the user
    if (user_access('administer context help')) {
      $header[] = array('data' => t('NID'), 'field' => 'nid');
    }
    $header[] = array('data' => t('Url'), 'field' => 'url');
    $header[] = array('data' => t('Title'), 'field' => 'title');

    // building the query pager so we can run the sql query
    $rowcount = variable_get('contexthelp_pager_count', 20);
    $count_sql = sprintf("SELECT DISTINCT nid FROM {node} WHERE type = '%s'", $contexthelp_type);
    $sql = sprintf("SELECT nid FROM {node} WHERE type = '%s'". tablesort_sql($header), $contexthelp_type);

    // running the query and processing the returned rows
    $result = pager_query($sql, $rowcount, NULL, $count_sql);
    while ($row = db_fetch_object($result)) {
      $node = node_load($row->nid);
      if (user_access('administer context help')) {
        $entry[$node->nid]['nid'] = l($node->nid, 'node/'. $node->nid .'/edit');
      }
      $entry[$node->nid]['url'] = $node->field_url[0]['value'];
      $entry[$node->nid]['title'] = $node->title;
    }

    $output = theme('table', $header, $entry);
    $output .= theme('pager', NULL, $rowcount, 0);
  }
  return $output;
}

/**
 * Function for creating the Export Help Form
 */
function contexthelp_export_help_form(&$form_state) {
  $form = array();
  $contexthelp_type = variable_get('contexthelp_node_type', '');
  if (!is_null($contexthelp_type) && drupal_strlen($contexthelp_type) > 0) {
    if (!isset($form_state['storage']['export_nodes'])) {
      $options = array();
      $result = db_query("SELECT nid FROM {node} WHERE type = '%s'", $contexthelp_type);
      while ($row = db_fetch_object($result)) {
        $node = node_load($row->nid);
        $options[$row->nid] = $node->title .' ('. $node->field_url[0]['value'] .')';
      }
      $form['export_nodes'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Context Help Export'),
        '#options' => $options,
        '#description' => t('Choose the help content you wish to export from the system'),
      );
      $form['submit'] = array('#type' => 'submit', '#value' => t('Export'));
    }
    else {
      // Form was submitted so now we want too retrieve the nodes and display the XML
      $nids = array_values($form_state['storage']['export_nodes']);
      if (!empty($nids)) {
        $default_locale = variable_get('contexthelp_default_locale', 'en');
        $output = "<ContextHelp>\n";
        foreach ($nids as $nid) {
          // If 0 then it was not checked we don't want this one
          if ($nid === 0) { continue; }
          $node = node_load($nid);
          $output .= "<HelpElement>\n";
          $output .= "<helpTitle>". $node->title ."</helpTitle>\n";
          $output .= "<helpBody>". $node->body ."</helpBody>\n";
          if (isset($node->language) && !is_null($node->language) && drupal_strlen($node->language) > 0) {
            $output .= "<helpLanguage>". $node->language ."</helpLanguage>\n";
          }
          else {
            $output .= "<helpLanguage>". $default_locale ."</helpLanguage>\n";
          }
          $output .= "<helpUrl>". $node->field_url[0]['value'] ."</helpUrl>\n";
          $output .= "</HelpElement>\n";
        }
        $output .= "</ContextHelp>\n";
      }
      $form['export_nodes'] = array(
        '#type' => 'textarea',
        '#title' => t('Context Help'),
        '#default_value' => $output,
        '#description' => t('Context Help in the format that can be imported back into the system'),
        '#rows' => 30,
      );
    }
  }
  else {
    drupal_set_message(t('Context Help Type has not been defined in the system'), 'error');
  }
  return $form;
}

/**
 * Form submission handler for the contexthelp export help form
 */
function contexthelp_export_help_form_submit(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['storage']['export_nodes'] = $form_state['values']['export_nodes'];
}

/**
 * Function for creating the Import Help Form
 */
function contexthelp_import_help_form(&$form_state) {
  $form = array();
  if (function_exists('xml_parser_create')) {
    $contexthelp_type = variable_get('contexthelp_node_type', '');
    if (!is_null($contexthelp_type) && drupal_strlen($contexthelp_type) > 0) {
      $form['import'] = array(
        '#type' => 'textarea',
        '#title' => t('Import Context Help'),
        '#description' => t('Import Context Help into the system'),
        '#rows' => 30,
      );
      $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
    }
    else {
      drupal_set_message(t('Context Help Type has not been defined in the system'), 'error');
    }
  }
  else {
    drupal_set_message(t('PHP does not have the required XML library installed cannot import help'), 'error');
  }
  return $form;
}

/**
 * Form submission handler for the contexthelp import form
 */
function contexthelp_import_help_form_submit(&$form, &$form_state) {
  global $user;
  // Check to make sure something actually was entered into the form
  if (isset($form_state['values']['import']) && !is_null($form_state['values']['import']) && drupal_strlen($form_state['values']['import']) > 0) {
    $parser = xml_parser_create('');
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($form_state['values']['import']), $xml_values);
    xml_parser_free($parser);

    $array = array();
    $count = 0;
    foreach ($xml_values as $k => $v) {
      if ($v['tag'] == 'helpTitle') {
        $array[$count] = array('title' => $v['value']);
      }
      if ($v['tag'] == 'helpBody') {
        $array[$count]['body'] = $v['value'];
      }
      if ($v['tag'] == 'helpLanguage') {
        $array[$count]['language'] = $v['value'];
      }
      if ($v['tag'] == 'helpUrl') {
        $array[$count]['url'] = $v['value'];
        $count++;
      }
    }
    // Data should now be in the correct format so we can create the pertinent nodes in the system
    if (!empty($array)) {
      $contexthelp_type = variable_get('contexthelp_node_type', '');
      $default_locale = variable_get('contexthelp_default_locale', 'en');
      foreach ($array as $k => $v) {
        $node = new StdClass();
        $node->uid = $user->uid;
        $node->type = $contexthelp_type;
        $node->title = $v['title'];
        $node->body = $v['body'];
        if (isset($v['language']) && !is_null($v['language']) && drupal_strlen($v['language']) > 0) {
          $node->language = $v['language'];
        }
        else {
          $node->language = $default_locale;
        }
        $node->field_url[0]['value'] = $v['url'];
        $node = node_submit($node);
        node_save($node);
      }
    }
    else {
      drupal_set_message(t('Mal-formed / invalid document received by the Import function'), 'error');
    }
    drupal_set_message(t('Context Help has been successfully imported into the system'));
  }
  else {
    drupal_set_message(t('Invalid / No Context Help information was entered'), 'error');
  }
}

